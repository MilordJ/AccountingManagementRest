/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.controller;

import com.neuron.cm.Exception.QException;
import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.OperationResponse;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.enums.ResultCode;
//import com.neuron.sms.rs.Exception.QException;
import com.neuron.sms.rs.db.DBConnect;
//import com.neuron.sms.rs.domain.Carrier;
//import com.neuron.sms.rs.domain.OperationResponse;
//import com.neuron.sms.rs.domain.SessionManager;
import com.neuron.sms.rs.enums.RequiredConstants;
//import com.neuron.sms.rs.enums.ResultCode;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.dbutils.DbUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author otahmadov
 */

@RestController
@RequestMapping(value = "/dispatcher", produces = MediaType.APPLICATION_JSON_VALUE)
public class DispatcherController {
    
    @Autowired
    private DBConnect dbConnect;
    
//    @Autowired
//    private DBConnect2 dbConnect2;
    
    @Autowired
    private RequiredConstants constants;
    
    @RequestMapping(value = "", method = RequestMethod.POST)
    protected OperationResponse forwardLocalCtrl(HttpServletRequest request) throws QException, SQLException, Throwable {
        String ctrl = "";
        String method = "";
        String orgId = "";
        String uniId = "";
        String orgFormula = "";
        String userType = "";
        Carrier result = new Carrier();
        OperationResponse operationResponse = new OperationResponse(ResultCode.ERROR);
        Connection connection = dbConnect.getConnection();
        try {
            ctrl= request.getParameter("ctrl");
            method= request.getParameter("method");
            orgId= request.getParameter("orgId");
            uniId= request.getParameter("uniId");
            orgFormula= request.getParameter("orgFormula");
            userType= request.getParameter("userType");
            String json = request.getParameter("json");
            String userId = request.getParameter("userId");
//            SessionManager.SCHEMA_PREFIX = request.getParameter("schemaName");
            SessionManager manager = new SessionManager();
            long threadId = Thread.currentThread().getId();
            Carrier carrier = new Carrier();
            carrier.fromJson(json);
            String lang = carrier.getValue("lang") != null && !carrier.getValue("lang").toString().trim().isEmpty() ? carrier.getValue("lang").toString().trim() : "az";
            manager.setCarrier(threadId, carrier);
            manager.setConnection(threadId, connection);
            manager.setUserId(threadId, userId);
            manager.setUserOrg(threadId, orgId);
            manager.setUserUni(threadId, uniId);
            manager.setUserOrgFormula(threadId, orgFormula);
            manager.setUserType(threadId, userType);
            manager.setLang(threadId, lang);
            manager.setRequiredConstants(constants);
            callService(method, ctrl);
            result = SessionManager.sessionCarrier();
            operationResponse.setCode(ResultCode.OK);
            operationResponse.setData(result.getJson());
        } 
        catch (Exception e) {
            e.printStackTrace();
            
        } finally {
            SessionManager.cleanSessionThread();
            if (connection != null) connection.close();
            if (connection != null) DbUtils.close(connection);
//            super.finalize();
            
            
        }
        
       return  operationResponse;
    }
    
    private static void callService(String methodName, String ctrlName) throws Exception {
            
            executeControllMethods(methodName, ctrlName);
            
            if (!SessionManager.sessionCarrier().hasError()) {
                executeModelMethods(methodName, ctrlName);
            }   else {
//                throw new Exception("Has error!!!");
            }
    }

    private static void executeControllMethods(String methodName, String ctrlName) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException {
        Class cl = Class.forName("com.neuron.sms.rs.validation." + ctrlName + "Validator");
        Object o = cl.newInstance();
        Method m = cl.getMethod(methodName);
        m.invoke(o);
    }


    private static void executeModelMethods(String methodName, String ctrlName) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException {
        Class cl = Class.forName("com.neuron.sms.rs.model." + ctrlName + "Model");
        Object o = cl.newInstance();
        Method m = cl.getMethod(methodName);
        m.invoke(o);
    }
    
}
