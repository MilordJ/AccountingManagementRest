/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.enums;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author otahmadov
 */
@Component
@PropertySource("classpath:required.properties")
@ConfigurationProperties
@Data
public class RequiredConstants {
    
    private String insertPaymenParts;
    private String updatePaymenParts;
    private String deletePaymenParts;

    private String insertCoursePayments;
    private String updateCoursePayments;
    private String deleteCoursePayments;
    
    private String insertStructurePayments;
    private String updateStructurePayments;
    private String deleteStructurePayments;

    private String insertPelcInstallments;
    private String updatePelcInstallments;
    private String deletePelcInstallments;
}
