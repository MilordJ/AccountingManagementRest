/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.enums;

/**
 *
 * @author otahmadov
 */
public class DictionaryCode {
    public static final String ADMIT_ORDER = "ADM";
    
    public static final String TRANSFER_ORDER = "TRNS";
    public static final String TRANSFER_ORDER_EDU_TYPE = "TRNS_EDUTYPE";
    public static final String TRANSFER_ORDER_SABAH = "TRNS_SBH";
    public static final String TRANSFER_ORDER_SPEC = "TRNS_SPEC";
    public static final String TRANSFER_ORDER_ALL = "TRNS_ALL";
    
    public static final String BREAK_ORDER = "BRK";
    public static final String RESTORATION_ORDER = "RSTR";
    public static final String YDA_ORDER = "YDA";
    public static final String DIPLOM_ORDER = "DPLM";
    public static final String DROP_ORDER = "DRP";
    
    
    public static final String SIMPLE_DIPLOM = "DPLM_SMPL";
    public static final String DISTINCTION_DIPLOM = "DPLM_DSTN";
}
