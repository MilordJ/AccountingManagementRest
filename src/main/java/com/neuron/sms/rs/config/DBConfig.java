/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.core.env.Environment;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 *
 * @author Orkhan
 */
@Configuration
@EnableMBeanExport(registration=RegistrationPolicy.IGNORE_EXISTING)
public class DBConfig {
    
    @Autowired
    private Environment env;
    
    private static Map<String, String> configMap = new HashMap<>();
    
    
        private static void convertFileToMap(String configPath) {
                try{
//                 List<String> allLines = Files.readAllLines(Paths.get(System.getProperty("user.dir") + configPath));
                 List<String> allLines = Files.readAllLines(Paths.get(configPath));
                                for (String line : allLines) {
                                        if(line != null && !line.trim().isEmpty()) {
                                            String key = line.split("=")[0];
                                            String value = line.substring(key.length() + 1);
                                            configMap.put(key, value);
                                        }
                                }
                                
                                
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } 
    
//    @Bean(name = "datasource1")
//    @Primary
//    public DataSource dataSource() {
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//            dataSourceBuilder.url(env.getProperty("jdbc.url"));
//            dataSourceBuilder.username(env.getProperty("jdbc.username"));
//            dataSourceBuilder.password(env.getProperty("jdbc.password"));
//            return dataSourceBuilder.build();   
//    }
//    
//    @Bean(name = "datasource2")
//    public DataSource dataSource2() {
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//            dataSourceBuilder.url(env.getProperty("jdbc.url"));
//            dataSourceBuilder.username(env.getProperty("jdbc.username"));
//            dataSourceBuilder.password(env.getProperty("jdbc.password"));
//            return dataSourceBuilder.build();   
//    }
//    @Bean
//    public DataSource dataSource() {
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//            dataSourceBuilder.url(env.getProperty("jdbc.url"));
//            dataSourceBuilder.username(env.getProperty("jdbc.username"));
//            dataSourceBuilder.password(env.getProperty("jdbc.password"));
//            return dataSourceBuilder.build();   
//    }
    
    @Bean
    public DataSource dataSource() {
         HikariConfig config = new HikariConfig();
         config.setPoolName("permission_hikari_datasource");
         config.setMaximumPoolSize(Integer.parseInt(env.getProperty("hikari.maxPoolSize")));
         config.setMaxLifetime(Integer.parseInt(env.getProperty("hikari.maxLifetime")));
         config.setMinimumIdle(Integer.parseInt(env.getProperty("hikari.minimumIdle")));
         config.setIdleTimeout(Integer.parseInt(env.getProperty("hikari.idleTimeout")));
         config.setConnectionTimeout(Integer.parseInt(env.getProperty("hikari.connectionTimeout")));
         config.setLeakDetectionThreshold(Integer.parseInt(env.getProperty("hikari.leakDetectionThreshold")));
         config.setConnectionTestQuery(env.getProperty("hikari.connectionTestQuery"));
         config.setDriverClassName(env.getProperty("hikari.driverClassName"));
         config.setJdbcUrl(env.getProperty("hikari.jdbc.url"));
         config.addDataSourceProperty("user", env.getProperty("hikari.jdbc.username"));
         config.addDataSourceProperty("password", env.getProperty("hikari.jdbc.password"));
         
         String configPath = env.getProperty("config.path");
        System.out.println("---------1111111111111111   " + configPath);
        convertFileToMap(configPath);
        
//         HikariConfig config = new HikariConfig();
//         config.setPoolName("dispatcher_hikari_datasource2");
//         config.setMaximumPoolSize(Integer.parseInt(configMap.get("hikari.maxPoolSize")));
//         config.setMaxLifetime(Integer.parseInt(configMap.get("hikari.maxLifetime")));
//         config.setMinimumIdle(Integer.parseInt(configMap.get("hikari.minimumIdle")));
//         config.setIdleTimeout(Integer.parseInt(configMap.get("hikari.idleTimeout")));
//         config.setConnectionTimeout(Integer.parseInt(configMap.get("hikari.connectionTimeout")));
//         config.setLeakDetectionThreshold(Integer.parseInt(configMap.get("hikari.leakDetectionThreshold")));
//         config.setConnectionTestQuery(configMap.get("hikari.connectionTestQuery"));
//         config.setDriverClassName(configMap.get("hikari.driverClassName"));
//         config.setJdbcUrl(configMap.get("hikari.jdbc.url"));
//         config.addDataSourceProperty("user", configMap.get("hikari.jdbc.username"));
//         config.addDataSourceProperty("password", configMap.get("hikari.jdbc.password"));

         return new HikariDataSource(config);
    }
}
