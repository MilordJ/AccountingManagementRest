/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.EntityAccountingPaymentParts;
import com.neuron.cm.domain.EntityAccountingStructurePayments;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_PaymentParts_v4;

/**
 *
 * @author otahmadov
 */
public class PaymentPartsModel {
    
     public void insertNewPaymentParts() throws Exception {
         EntityAccountingPaymentParts ent = new EntityAccountingPaymentParts();
         EntityManager.doInsert(ent);
    }
     
    public void updatePaymentParts() throws Exception {
        EntityAccountingPaymentParts ent = new EntityAccountingPaymentParts();
        EntityManager.doUpdate(ent);
    }
    
    public void deletePaymentParts() throws Exception {
        EntityAccountingPaymentParts ent = new EntityAccountingPaymentParts();
        EntityManager.doDelete(ent);
    }
    
    public void getPaymentPartsList() throws Exception {
        EntityAccountingV_PaymentParts_v4 ent = new EntityAccountingV_PaymentParts_v4();
        EntityManager.doSelect(ent);
    }
}
