/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.sms.rs.validation.*;
import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityAccountingCoursePayments;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_CoursePayments_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class CoursePaymentsModel {
    
    public void insertNewCoursePayments() throws Exception {
         EntityAccountingCoursePayments ent = new EntityAccountingCoursePayments();
         EntityManager.doInsert(ent);
    }
    public void updateCoursePayments() throws Exception {
         EntityAccountingCoursePayments ent = new EntityAccountingCoursePayments();
         EntityManager.doUpdate(ent);
    }
    public void deleteCoursePayments() throws Exception {
         EntityAccountingCoursePayments ent = new EntityAccountingCoursePayments();
         EntityManager.doDelete(ent);
    }
    public void getCoursePaymentsList() throws Exception {
         EntityAccountingV_CoursePayments_v4 ent = new EntityAccountingV_CoursePayments_v4();
         EntityManager.doSelect(ent);
    }
    
}
