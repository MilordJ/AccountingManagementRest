/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.EntityAccountingStructurePayments;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_StructurePayments_v4;

/**
 *
 * @author otahmadov
 */
public class StructurePaymentsModel {
     public void insertNewStructurePayments() throws Exception {
         EntityAccountingStructurePayments ent = new EntityAccountingStructurePayments();
         EntityManager.doInsert(ent);
    }
     
    public void updateStructurePayments() throws Exception {
        EntityAccountingStructurePayments ent = new EntityAccountingStructurePayments();
        EntityManager.doUpdate(ent);
    }
    
    public void deleteStructurePayments() throws Exception {
        EntityAccountingStructurePayments ent = new EntityAccountingStructurePayments();
        EntityManager.doDelete(ent);
    }
    
    public void getStructurePaymentsList() throws Exception {
        EntityAccountingV_StructurePayments_v4 ent = new EntityAccountingV_StructurePayments_v4();
        EntityManager.doSelect(ent);
    }
}
