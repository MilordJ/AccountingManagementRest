/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.model;

import com.neuron.cm.domain.EntityAccountingPelcInstallments;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_PelcInstallments_v4;

/**
 *
 * @author Lito
 */
public class PelcInstallmentsModel {
    
    public void insertNewPelcInstallments() throws Exception {
         EntityAccountingPelcInstallments ent = new EntityAccountingPelcInstallments();
         EntityManager.doInsert(ent);
    }
    public void updatePelcInstallments() throws Exception {
         EntityAccountingPelcInstallments ent = new EntityAccountingPelcInstallments();
         EntityManager.doUpdate(ent);
    }
    public void deletePelcInstallments() throws Exception {
         EntityAccountingPelcInstallments ent = new EntityAccountingPelcInstallments();
         EntityManager.doDelete(ent);
    }
    public void getPelcInstallmentsList() throws Exception {
         EntityAccountingV_PelcInstallments_v4 ent = new EntityAccountingV_PelcInstallments_v4();
         EntityManager.doSelect(ent);
    }
    
}
