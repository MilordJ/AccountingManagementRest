package com.neuron.sms.rs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class PermissionRestApplication {

	public static void main(String[] args) {
            
		SpringApplication.run(PermissionRestApplication.class, args);
                
	}
}
