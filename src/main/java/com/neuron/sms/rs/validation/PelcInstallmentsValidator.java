/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityAccountingPelcInstallments;
import com.neuron.cm.domain.SessionManager;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class PelcInstallmentsValidator {
    Class domainAccountingPelcInstallments = EntityAccountingPelcInstallments.class;
    
    public void insertNewPelcInstallments() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainAccountingPelcInstallments);
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertPelcInstallments());
        String uniId = SessionManager.getCurrentUserUni();
        c.setValue("orgId", uniId);
    }
    public void updatePelcInstallments() throws Exception {        
        ChangeFieldFormatValidator.changeDateFormat(domainAccountingPelcInstallments);
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdatePelcInstallments());
        String uniId = SessionManager.getCurrentUserUni();
        c.setValue("orgId", uniId);
    }
    public void deletePelcInstallments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeletePelcInstallments());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void getPelcInstallmentsList() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    
}
