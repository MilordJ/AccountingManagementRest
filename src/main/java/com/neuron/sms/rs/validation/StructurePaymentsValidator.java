/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_PelcInstallments_v4;
import com.neuron.cm.view.EntityAccountingV_StructurePayments_v4;
 import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class StructurePaymentsValidator {
    public void insertNewStructurePayments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertStructurePayments());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void updateStructurePayments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateStructurePayments());
         String id = c.getValue("id").toString();
         String eduYearId = c.getValue("eduYearId").toString();
         Carrier carrier = new Carrier();
         carrier.setValue("id", id);
         carrier.setValue("eduYearId", eduYearId);
         EntityAccountingV_StructurePayments_v4 v_StructurePayments_v4 = new EntityAccountingV_StructurePayments_v4();
         String specOrgId = EntityManager.getColumnValueByColumnName(v_StructurePayments_v4, "orgId", carrier);
         EntityAccountingV_PelcInstallments_v4 pelcInstallments_v4 = new EntityAccountingV_PelcInstallments_v4();
         carrier = new Carrier();
         carrier.setValue("specOrgId", specOrgId);
         carrier.setValue("eduYearId", eduYearId);
         int rowCount = EntityManager.getRowCount(pelcInstallments_v4, carrier);
         if(rowCount>0){
             c.addError("invalidSpec", "İxtisas ödəniş üçün istifadə edilib!");
             return;
         }
         
         String uniId = SessionManager.getCurrentUserUni();
         carrier = new Carrier();
         carrier.setValue("id", id);
//         String orgId = EntityManager.getColumnValueByColumnName(v_StructurePayments_v4, "orgId", carrier);
//         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
//             c.addError("Id", "Invalid Payment Part id!!!");
//             return;
//         }
    }
        
    public void deleteStructurePayments() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeletePaymenParts());
         String id = c.getValue("id").toString();
         Carrier carrier = new Carrier();
         EntityAccountingV_StructurePayments_v4 v_StructurePayments_v4 = new EntityAccountingV_StructurePayments_v4();
         String specId = EntityManager.getColumnValueByColumnName(v_StructurePayments_v4, "specId", carrier);
         String eduYearId = EntityManager.getColumnValueByColumnName(v_StructurePayments_v4, "eduYearId", carrier);
         carrier.setValue("id", id);
         carrier.setValue("eduYearId", eduYearId);
         EntityAccountingV_PelcInstallments_v4 pelcInstallments_v4 = new EntityAccountingV_PelcInstallments_v4();
         carrier = new Carrier();
         carrier.setValue("specId", specId);
         carrier.setValue("eduYearId", eduYearId);
         int rowCount = EntityManager.getRowCount(pelcInstallments_v4, carrier);
         if(rowCount>0){
             c.addError("invalidSpec", "İxtisas ödəniş üçün istifadə edilib!");
             return;
         }
         carrier = new Carrier();
         carrier.setValue("id", id);
         String orgId = EntityManager.getColumnValueByColumnName(v_StructurePayments_v4, "orgId", carrier);
         String uniId = SessionManager.getCurrentUserUni();
//         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
//             c.addError("Id", "Invalid Payment Part id!!!");
//             return;
//         }
    }
    
    public void getStructurePaymentsList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
}
