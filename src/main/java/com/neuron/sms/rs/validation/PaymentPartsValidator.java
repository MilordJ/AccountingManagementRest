/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityAccountingPaymentParts;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityAccountingV_PaymentParts_v4;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class PaymentPartsValidator {
    Class domainAccountingPaymentPart = EntityAccountingPaymentParts.class;
    
    public void insertNewPaymentParts() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainAccountingPaymentPart);
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertPaymenParts());
         String uniId = SessionManager.getCurrentUserUni();
         if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
    
    public void updatePaymentParts() throws Exception {
         ChangeFieldFormatValidator.changeDateFormat(domainAccountingPaymentPart);
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdatePaymenParts());
         String id = c.getValue("id").toString();
         EntityAccountingV_PaymentParts_v4 ent = new EntityAccountingV_PaymentParts_v4();
         Carrier carrier = new Carrier();
         carrier.setValue("id", id);
         String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
         String uniId = SessionManager.getCurrentUserUni();
         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
             c.addError("Id", "Invalid Payment Part id!!!");
             return;
         }
    }
        
    public void deletePaymentParts() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeletePaymenParts());
         String id = c.getValue("id").toString();
         EntityAccountingV_PaymentParts_v4 ent = new EntityAccountingV_PaymentParts_v4();
         Carrier carrier = new Carrier();
         carrier.setValue("id", id);
         String orgId = EntityManager.getColumnValueByColumnName(ent, "orgId", carrier);
         String uniId = SessionManager.getCurrentUserUni();
         if(orgId == null || orgId.trim().isEmpty() || !orgId.equals(uniId)) {
             c.addError("Id", "Invalid Payment Part id!!!");
             return;
         }
    }
    
    public void getPaymentPartsList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
}
