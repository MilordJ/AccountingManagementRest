/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import com.neuron.sms.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class CoursePaymentsValidator {
    
    public void insertNewCoursePayments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertCoursePayments());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void updateCoursePayments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateCoursePayments());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void deleteCoursePayments() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteCoursePayments());
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    public void getCoursePaymentsList() throws Exception {
         Carrier c = SessionManager.sessionCarrier();
         String uniId = SessionManager.getCurrentUserUni();
         c.setValue("orgId", uniId);
    }
    
}
